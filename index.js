import { functionTypeAnnotation } from "babel-types";

var locations = {
    moscow: [55.746215, 37.622504],
    spb: [59.939095, 30.315868],
    ufa: [54.735147, 55.958727],
    nn: [56.326797, 44.006516],
    voronezh: [51.660781, 39.200269],
    grozniy: [43.317881, 45.695358],
    ekb: [56.838011, 60.597465],
    novosib: [55.030199, 82.920430],
    krasnodar: [45.035470, 38.975313],
    krasnogorsk: [55.831099, 37.330192],
    rnd: [47.222078, 39.720349],
    mytishchi: [55.910483, 37.736402],
    balashiha: [55.796339, 37.938199],
    stavropol: [45.044521, 41.969083],
    kaliningrad: [54.710454, 20.512733],
    penza: [53.195063, 45.018316],
    sochi: [43.585525, 39.723062],
    yakutsk: [62.027757, 129.731235],
    tyumen: [57.153033, 65.534328],
    chelyabinsk: [55.159897, 61.402554],
    vladikavkaz: [43.021150, 44.681960],
    surgut: [61.254035, 73.396221],
    ulyanovsk: [54.314192, 48.403123],
    nizhnevartovsk: [60.938545, 76.558902],
    gelendzhik: [44.561141, 38.076809],
    blagoveschensk: [50.290640, 127.527173]
};

window.locations = locations;

$(document).ready(function() {
    if (!window.city) {
        window.city = "moscow"
    }

    $('input[type=file]').change(function(e) {
        $in = $(this);
        $in.next().html($in.val());

    });

    $('.uploadButton').click(function() {
        var fileName = $("#fileUpload").val();
        if (fileName) {
            alert(fileName + " can be uploaded.");
        } else {
            alert("Please select a file to upload");
        }
    });


    $('#qr-code').qrcode({
        text: "http://www.google.com",
        width: 100,
        height: 100,
        background: "#ffffff",
        foreground: "#000000"
    });

    $(".list-of-cities li").click(function(e) {
        e.preventDefault();
        $(".list-of-cities li").removeClass('city-active');
        $(this).addClass('city-active');

        // $(".dropdown .btn").html($(this).text());

        // $(".dropdown .btn").append('<span><img src="/img/arrow-down.svg" height="9px" width="16px /"></span>')


        var selectedLocation = $(this).data('location');

        map.setCenter(locations[selectedLocation]);
        map.setZoom(12);
        window.city = selectedLocation
    })

    $('#dropdown').change(function() {
        var val = $("#dropdown option:selected").attr('data-location');
        map.setCenter(locations[val]);
        map.setZoom(12);
        window.city = val;
    });

    $('.menu__burger').on('click', function(e) {
        e.preventDefault;
        $(this).toggleClass('menu__burger--active');
        $('.menu-popup').toggleClass('active');
    })

    $(window).scroll(hideBlock);

    function hideBlock() {
        $('.menu-popup').removeClass('active');
        $('.menu__burger').removeClass('menu__burger--active')
    }

    $('#checkbox').on('change', function() {
        if ($('#checkbox').prop('checked')) {
            $('#submit').attr('disabled', false);
        } else {
            $('#submit').attr('disabled', true);
        }
    });

    let inputsArrId = ['nameInput', 'emailInput', 'themeMessageSelect', 'messageInput']
    let inputs = {
        name: false,
        email: false,
        themMessage: false,
        message: false
    }

    $('#nameInput').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs.name === false) {
            for (let i = 0; i <= inputsArrId.length; i++) {
                if ($(this).attr('id') === inputsArrId[i]) {
                    inputsArrId.splice(i, 1)

                }
            }
            inputs.name = true
        }
        if ($(this).val().length === 0 && inputs.name === true) {
            inputs.name = false
            inputsArrId.push($(this).attr('id'))
        }
    })

    $('#emailInput').on('change keyup paste', function() {
        $(this).removeClass('error')

        if ($(this).val().length > 0 && inputs.email === false) {
            inputs.email = true
            for (let i = 0; i <= inputsArrId.length; i++) {
                if ($(this).attr('id') === inputsArrId[i]) {
                    inputsArrId.splice(i, 1)
                }
            }
        }
        if ($(this).val().length === 0 && inputs.email === true) {
            inputs.email = false
            inputsArrId.push($(this).attr('id'))
        }
    })

    $('#themeMessageSelect').on('change keyup paste', function() {
        $(this).removeClass('error')

        if ($(this).val().length > 0 && inputs.themMessage === false) {
            inputs.themMessage = true
            for (let i = 0; i <= inputsArrId.length; i++) {
                if ($(this).attr('id') === inputsArrId[i]) {
                    inputsArrId.splice(i, 1)

                }
            }
        }
        if ($(this).val().length === 0 && inputs.themMessage === true) {
            inputs.themMessage = false
            inputsArrId.push($(this).attr('id'))
        }
    })

    $('#messageInput').on('change keyup paste', function() {
        $(this).removeClass('error')

        if ($(this).val().length > 0 && inputs.message === false) {
            inputs.message = true
            for (let i = 0; i <= inputsArrId.length; i++) {
                if ($(this).attr('id') === inputsArrId[i]) {
                    inputsArrId.splice(i, 1)

                }

            }
        }
        if ($(this).val().length === 0 && inputs.message === true) {
            inputs.message = false
            inputsArrId.push($(this).attr('id'))
            console.log(inputsArrId)
        }
    })


    $('#submit').on('click', function(e) {
        for (let i = 0; i <= inputsArrId.length; i++) {
            $('#' + inputsArrId[i] + '').addClass('error')
        }
        if (inputsArrId.length == 0) {
            $('#feedback form').addClass('no-active');
            $('#feedback .success').removeClass('no-active');
            $('#nameInput, #emailInput, #themeMessageSelect, #messageInput').val('')
            inputsArrId = ['nameInput', 'emailInput', 'themeMessageSelect', 'messageInput']
            inputs = {
                name: false,
                email: false,
                themMessage: false,
                message: false
            }
        }
    })

    document.addEventListener('invalid', (function() {
        return function(e) {
            e.preventDefault();
        };
    })(), true);

    $('.js-slider').slick({
        dots: false,
        infinite: false,
        speed: 300,
        variableWidth: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        prevArrow: '<button class="arrow arrow--prev"></button>',
        nextArrow: '<button class="arrow arrow--next"></button>',
        responsive: [{
                breakpoint: 1280,
                settings: {
                    prevArrow: '<button class="arrow arrow--prev"></button>',
                    nextArrow: '<button class="arrow arrow--next"></button>',
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    variableWidth: true,
                    infinite: false,
                    dots: false
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    prevArrow: '<button class="arrow arrow--prev"></button>',
                    nextArrow: '<button class="arrow arrow--next"></button>',
                    slidesToScroll: 3,
                    variableWidth: true,
                    infinite: false,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    prevArrow: '<button class="arrow arrow--prev"></button>',
                    nextArrow: '<button class="arrow arrow--next"></button>',
                    variableWidth: true,
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    prevArrow: '<button class="arrow arrow--prev"></button>',
                    nextArrow: '<button class="arrow arrow--next"></button>',
                    variableWidth: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

})